const cmd = require('node-cmd');
const path = require('path');

const dirPath = process.argv[2]
const pathToStart = path.resolve(dirPath, 'start.circ');

const command = path.resolve('logisim-win-2.7.1.exe') + ' '
    + path.resolve(pathToStart);

console.log(command);
if (!dirPath) return;

cmd.get(
    command,
    function (err, data, stderr) {
        console.log(data);
        if (err) console.log(err);
    }
);